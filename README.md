# Arquitectura de Microservicios Mixta

Este desarrollo demustra la aplicación de una arquitectura de microservicios, considerando trazado de llamadas para tener visibilidad del sistema.

Un usuario visualiza el precio de cada ícono a través de una interfaz web. El precio de cada ícono es deteminado por los valores diarios del dólar y el euro.

Se puede consultar un despliegue del sistema en la máquina de AWS EC2

http://ec2-54-207-105-175.sa-east-1.compute.amazonaws.com

La interfaz web se entrega en el puerto 8080 y el tracer en el puerto 16686.

WEB
- http://ec2-54-207-105-175.sa-east-1.compute.amazonaws.com:8080

TRACER
- http://ec2-54-207-105-175.sa-east-1.compute.amazonaws.com:16686

RESTful JSON API
- http://ec2-54-207-105-175.sa-east-1.compute.amazonaws.com:9090

GRPC endpoint
- http://ec2-54-207-105-175.sa-east-1.compute.amazonaws.com:6565

## Deploy con Docker

El sistema está orquestado a través de Docker Compose. Se compone de una serie de servicios gRPC, una interfaz web.

Cada componente posee un mecanismo de construcción independiente para agilizar su automatización en CI/CD y .

### Levantar el entorno en una máquina virtual

Se asume un máquina con git, docker y docker-compose instalados.

git clone https://gitlab.com/mancvso/revision-bice
cd revision-bice
git checkout dev
docker-compose up

Luego de construir, quedan disponibles la interfaz web y el tracer.

## Interoperabilidad de Interfaces

Las definiciones de los clientes y servidores de los servicios que componen el sistema, están aisladas en una librería Java (para los microservicios Java). Sin embargo, están basadas en gRPC+Protobuf, lo que permite tener implementar clientes y servidores prácticamente en cualquier lenguaje de programación.

Tecnologías:
- gRPC
- Protobuf
- Gradle

## Trazabilidad

Este ejemplo demuestra la visibilidad que se obtiene al implementar sistemas de traza en este tipo de arquitecturas.

La interfaz web realiza una llamada al servicio RESTful Icons, el cual realiza dos llamadas: una al microservicio Alerts (quien interactúa con Redis) y otra llamada auna API externa.

Estas interacciones y dependencias quedan al descubierto en el trazador.

Tecnologías:
- OpenTelemetry
- (Uber) Jaeger

## Microservicios

Un conjunto simple de servicios gRPC basados en Java, Spring Boot y Redis.

Tecnologías:
- Reactor Core
- gRPC
- Multi-stage Docker build
- Spring Boot
- Redis
- Gradle

### Alerts

Entrega un servicio gRPC a través del cual se puede consultar e insertar nuevos eventos.

### Icons

Entrega una API RESTful JSON (sólo GET en este ejemplo), la que se encarga de consultar un servicio externo y calcular los precios de cada ícono.

## Web UI

Esta interfaz está disponible en el puerto 4200. Muestra los últimos sucesos del sistema.

Tecnologías:
- Angular 9
- Servidor web NGINX
- Multi-stage Docker build

## Jitpack

Este servicio web SaaS nos permite usar el código fuente de un projecto como librería Gradle/Maven. Lo usamos para mantener independiente la construcción de los microservicios, con tal de simplificar el pipeline de CI/CD.

https://jitpack.io/#com.gitlab.mancvso/revision-bice-lib/dev-SNAPSHOT