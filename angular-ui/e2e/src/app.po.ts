import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl) as Promise<unknown>;
  }

  getTitleText(): Promise<string> {
    return browser.executeScript("return arguments[0].innerHTML;", element(by.css('app-root .content span'))) as Promise<string>;
    //return element(by.css('app-root .content span')).getInnerHtml() as Promise<string>;
  }
}
