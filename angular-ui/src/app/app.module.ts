import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent, BottomSheetOverviewExampleSheet } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './reducers';
import { DailyComponent } from './daily/daily.component';
import { MatBottomSheetModule, MatBottomSheet } from '@angular/material/bottom-sheet';
import { FullMaterialModule } from './material.module';

import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    BottomSheetOverviewExampleSheet,
    DailyComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    StoreModule.forRoot({}, {}),
    StoreModule.forRoot(reducers, {
      metaReducers
    }),
    FullMaterialModule,
    HttpClientModule
  ],
  providers: [MatBottomSheetModule, MatBottomSheet],
  bootstrap: [AppComponent],
  exports: [
    // Material
  ],
  entryComponents: [BottomSheetOverviewExampleSheet]
})
export class AppModule { }
