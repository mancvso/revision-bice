import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class IconsService {
  constructor(private http: HttpClient) {  }

  endpoint:String = 'http://ec2-54-207-105-175.sa-east-1.compute.amazonaws.com:9090/api/v1/';

  getIcons(): Observable<any> {
    return this.http.get(this.endpoint + 'icons').pipe( map(this.extractData) );
  }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }
}
