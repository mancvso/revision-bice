package cl.bice.core.alerts;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import lombok.Data;

@Data
@RedisHash("alerts")
class AlertEntry {
  @Id String id;
  @Indexed String title;
  @Indexed String message;
  // google.protobuf.Timestamp created  = 4;
}