package cl.bice.core.alerts;

public class AlertsConstants {
    public static final String REDIS_HOST = "redis";
    public static final int REDIS_PORT = 6379;
}