package cl.bice.core.alerts;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
interface AlertRepository extends CrudRepository<AlertEntry, String> { }