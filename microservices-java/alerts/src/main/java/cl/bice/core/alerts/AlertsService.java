package cl.bice.core.alerts;

import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;

import reactor.core.publisher.Mono;
import cl.bice.services.alerts.Alert;
import cl.bice.services.alerts.CreateAlertRequest;
import cl.bice.services.alerts.ListAlertsResponse;
import cl.bice.services.alerts.ReactorAlertServiceGrpc;

@GRpcService
//@CommonsLog
public class AlertsService extends ReactorAlertServiceGrpc.AlertServiceImplBase {

  @Autowired
  AlertRepository alertRepository;

  public  Mono<cl.bice.services.alerts.ListAlertsResponse> listAlerts(Mono<cl.bice.services.alerts.ListAlertsRequest> request) {
    ListAlertsResponse.Builder response = ListAlertsResponse.newBuilder();
    for(AlertEntry entry: alertRepository.findAll()) {
      response.addAlerts(
        Alert.newBuilder()
        .setMessage(entry.message)
        .setTitle(entry.title)
        .build()
      );
    }
    return Mono.just(response.build());
  }
  
  public  Mono<Alert> getAlert(Mono<cl.bice.services.alerts.GetAlertRequest> request) {
    return null;
  }
  
  public  Mono<Alert> createAlert(Mono<cl.bice.services.alerts.CreateAlertRequest> request) {
    CreateAlertRequest createRequest = request.block();
    Alert.Builder response = Alert.newBuilder();
    AlertEntry entry = new AlertEntry();
    AlertEntry savedEntry = new AlertEntry();
    entry.setMessage(createRequest.getAlert().getMessage());
    entry.setTitle(createRequest.getAlert().getTitle());
    savedEntry = alertRepository.save(entry);
    response.setMessage(savedEntry.getMessage());
    response.setTitle(savedEntry.getTitle());
    return Mono.just(response.build());
  }
  
  public  Mono<Alert> updateAlert(Mono<cl.bice.services.alerts.UpdateAlertRequest> request) {
    return null;
  }
  
  public  Mono<com.google.protobuf.Empty> deleteAlert(Mono<cl.bice.services.alerts.DeleteAlertRequest> request) {
    return null;
  }
  
}