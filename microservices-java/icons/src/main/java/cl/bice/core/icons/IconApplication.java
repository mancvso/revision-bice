package cl.bice.core.icons;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IconApplication {

	@Autowired
	IconRepository iconsRepository;

	@PostConstruct
	private void populate() {
		// Populate app
		iconsRepository.save(new IconEntry("delete", 0.5d, 0.2d));
		iconsRepository.save(new IconEntry("book", 0.1d, 0.7d));
		iconsRepository.save(new IconEntry("favorite", 0.9d, 0.1d));
	}

	public static void main(String[] args) {
		SpringApplication.run(IconApplication.class, args);
	}
}
