package cl.bice.core.icons;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import lombok.Data;

@Data
@RedisHash("icons")
class IconEntry {
  @Id String id;
  @Indexed String name;
  @Indexed Double dolar;
  @Indexed Double euro;
  Double price; // Calculated

  public void calculatePrice(Price dolar, Price euro) {
    this.price = dolar.getValue() * this.dolar + euro.getValue() * this.euro;
  }

  public IconEntry(String name, double dolar, double euro) {
    this.name = name;
    this.dolar = dolar;
    this.euro = euro;
  }

}