package cl.bice.core.icons;

import lombok.Data;

@Data
public class IconPriceResult {
    Price dolar;
    Price euro;
}