package cl.bice.core.icons;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import lombok.extern.apachecommons.CommonsLog;

@CommonsLog
@Service
public class IconPriceService {
    public IconPriceResult getPrices() {
        RestTemplate restTemplate = new RestTemplate();
        IconPriceResult result = restTemplate.getForObject(IconsConstants.PRICES_API, IconPriceResult.class);
        log.info(result);
        return result;
    }
}