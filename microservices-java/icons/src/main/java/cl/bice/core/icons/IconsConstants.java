package cl.bice.core.icons;

public class IconsConstants {
    // Redis
    public static final String REDIS_HOST = "redis";
    public static final int REDIS_PORT = 6379;

    // Alerts service
    public static final String ALERTS_PORT = "6565";
    public static final String ALERTS_HOST = "alerts";

    // API
    public static final String RESTFUL_PATH = "/api/v1/icons";

    // Prices
    public static final String PRICES_API = "https://www.indecon.online/last";
}