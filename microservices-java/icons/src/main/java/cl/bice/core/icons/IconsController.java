package cl.bice.core.icons;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.extern.apachecommons.CommonsLog;

import cl.bice.clients.alerts.AlertsServiceClient;
import cl.bice.services.alerts.Alert;
import cl.bice.services.alerts.CreateAlertRequest;
import cl.bice.services.alerts.ReactorAlertServiceGrpc.ReactorAlertServiceStub;

@CommonsLog
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class IconsController {

    String host = IconsConstants.ALERTS_HOST;
    String port = IconsConstants.ALERTS_PORT;
    String target = String.join(":", host, port);
    ManagedChannel channel = ManagedChannelBuilder.forTarget(target).usePlaintext().build();

    ReactorAlertServiceStub alertsService = AlertsServiceClient.newClientFor(channel);

    @Autowired
    IconRepository iconsRepository;

    @Autowired
    IconPriceService iconsPriceService;

    @GetMapping(value = IconsConstants.RESTFUL_PATH)
    public List<IconEntry> getIcons() {

        List<IconEntry> icons = new ArrayList<>();
        
        Alert alert = Alert.newBuilder()
            .setTitle("Calling microservice")
            .setMessage("Retrieving list of icons")
            .build();
        CreateAlertRequest alertRequest = CreateAlertRequest.newBuilder()
            .setAlert(alert)
            .build();
        log.info("Call gRPC microservice");
        try {
            this.alertsService.createAlert(alertRequest);
        } catch (RuntimeException re){
            log.warn("Could send alert");
        }

        log.info("Call RESTful API");
        try {
            IconPriceResult prices = iconsPriceService.getPrices();
            for( IconEntry entry : iconsRepository.findAll() ){
                entry.calculatePrice( prices.getDolar(),  prices.getEuro() );
                icons.add(entry);
            }
            return icons;
        } catch (HttpClientErrorException ce) {
            log.warn("Could not update prices " + ce.getResponseBodyAsString());
        }
        // Return all data found on repository
        
        return icons;
    }

    @RequestMapping(method = RequestMethod.OPTIONS)
    public ResponseEntity<Boolean> checkCors() {
        HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Access-Control-Allow-Methods",
            "GET, OPTIONS");
 
        return ResponseEntity.ok().headers(responseHeaders).build();
    }
}