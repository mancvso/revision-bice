package cl.bice.core.icons;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
interface IconRepository extends CrudRepository<IconEntry, String> { }