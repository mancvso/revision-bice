package cl.bice.core.icons;

import lombok.Data;

@Data
public class Price {
    String key;
    Double value;
}